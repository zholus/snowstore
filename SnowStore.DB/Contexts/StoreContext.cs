using Microsoft.EntityFrameworkCore;
using SnowStore.Core.Entities;
using SnowStore.DB.EntitiesConfiguration;

namespace SnowStore.DB.Contexts
{
    public class StoreContext : DbContext
    {
        public const string CommonSchema = "all";

        public StoreContext(DbContextOptions options) : base(options)
        {}
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.HasDefaultSchema(CommonSchema);
            modelBuilder.ApplyConfiguration(new ProductEntityConfiguration());
        }

        public DbSet<Product> Products { get; set; }
        public DbSet<Category> Categories{ get; set; }
    }
}
