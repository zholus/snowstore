using System.Collections.Generic;
using System.Linq;
using SnowStore.Core.Entities;
using SnowStore.Core.Repositories;
using SnowStore.DB.Contexts;

namespace SnowStore.DB.Repositories
{
    public class ProductRepository : IProductRepository
    {
        private StoreContext context;
        
        public ProductRepository(StoreContext context)
        {
            this.context = context;
        }

        public bool Add(Product product)
        {
            context.Products.Add(product);

            return context.SaveChanges() > 0;
        }

        public List<Product> FindAllByCategory(Category category)
        {
            return context.Products
                .Select(x => x)
                .Where(x => x.Category.Equals(category))
                .ToList();
        }

        public Product FindOneBy(int productId)
        {
            return context.Products.Find(productId);
        }
    }
}