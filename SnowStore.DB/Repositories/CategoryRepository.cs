using System.Collections.Generic;
using System.Linq;
using SnowStore.Core.Entities;
using SnowStore.Core.Repositories;
using SnowStore.DB.Contexts;

namespace SnowStore.DB.Repositories
{
    public class CategoryRepository : ICategoryRepository
    {
        private StoreContext context;

        public CategoryRepository(StoreContext context)
        {
            this.context = context;
        }
        
        public List<Category> GetLimitedCategories(int limit = 5)
        {
            return context.Categories
                .Select(x => x)
                .Take(limit)
                .ToList();
        }

        public List<Category> GetAllCategories()
        {
            return context.Categories
                .Select(x => x)
                .ToList();
        }

        public bool Add(Category category)
        {
            context.Categories.Add(category);

            return context.SaveChanges() > 0;
        }

            public Category FindOneBy(int categoryId)
        {
            return context.Categories.Find(categoryId);
        }
    }
}