﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SnowStore.DB.Migrations
{
    public partial class Price : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Price",
                schema: "all",
                table: "Products",
                newName: "Price_Value");

            migrationBuilder.AddColumn<string>(
                name: "Price_Currency",
                schema: "all",
                table: "Products",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Price_Currency",
                schema: "all",
                table: "Products");

            migrationBuilder.RenameColumn(
                name: "Price_Value",
                schema: "all",
                table: "Products",
                newName: "Price");
        }
    }
}
