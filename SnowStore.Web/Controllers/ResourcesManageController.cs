using Microsoft.AspNetCore.Mvc;
using SnowStore.Core.Entities;
using SnowStore.Core.Repositories;
using SnowStore.Web.Builders.Entities;
using SnowStore.Web.ViewModels.ResourcesManage;

namespace SnowStore.Web.Controllers
{
    public class ResourcesManageController : Controller
    {
        private ICategoryRepository categoryRepository;
        private IProductRepository productRepository;
        private ProductEntityBuilder productEntityBuilder;
        
        public ResourcesManageController(
            ICategoryRepository categoryRepository, 
            IProductRepository productRepository,
            ProductEntityBuilder productEntityBuilder
        ) {
            this.categoryRepository = categoryRepository;
            this.productRepository = productRepository;
            this.productEntityBuilder = productEntityBuilder;
        }
        
        [Route("/resources/manage")]
        [HttpGet]
        public IActionResult Index()
        {
            ViewBag.categories = this.categoryRepository.GetAllCategories();
            return View();
        }

        [Route("/resources/manage/category")]
        [HttpPost]
        public IActionResult AddCategory(CategoryViewModel categoryViewModel)
        {
            var category = new Category
            {
                Name = categoryViewModel.Name
            };

            categoryRepository.Add(category);
            
            return RedirectToAction("Index");
        }

        [Route("/resources/manage/product")]
        [HttpPost]
        public IActionResult AddProduct(ProductViewModel productViewModel)
        {
            var product = productEntityBuilder.Build(productViewModel);

            productRepository.Add(product);
            
            return RedirectToAction("Index");
        }
    }
}