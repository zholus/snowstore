using Microsoft.AspNetCore.Mvc;
using SnowStore.Core.Repositories;

namespace SnowStore.Web.Controllers
{
    [Route("/product")]
    public class ProductController : Controller
    {
        private IProductRepository productRepository;
        
        public ProductController(IProductRepository productRepository)
        {
            this.productRepository = productRepository;
        }
        
        [Route("{productId:int}")]
        [HttpGet]
        public IActionResult Index(int productId)
        {
            var product = productRepository.FindOneBy(productId);

            ViewBag.product = product;
            
            return View();
        }
    }
}