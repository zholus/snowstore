using Microsoft.AspNetCore.Mvc;
using SnowStore.Core.Repositories;

namespace SnowStore.Web.Controllers
{
    public class CategoryController : Controller
    {
        private ICategoryRepository categoryRepository;
        private IProductRepository productRepository;
        public CategoryController(ICategoryRepository categoryRepository, IProductRepository productRepository)
        {
            this.categoryRepository = categoryRepository;
            this.productRepository = productRepository;
        }
        
        [Route("/category/{categoryId:int}")]
        [HttpGet]
        public IActionResult Index(int categoryId)
        {
            var category = categoryRepository.FindOneBy(categoryId);
            var products = productRepository.FindAllByCategory(category);
            
            ViewBag.category = category;
            ViewBag.products = products;
            
            return View();
        }
    }
}