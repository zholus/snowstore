using Microsoft.AspNetCore.Mvc;
using SnowStore.Core.Repositories;

namespace SnowStore.Web.Controllers
{
    public class HomeController : Controller
    {
        private ICategoryRepository categoryRepository;

        public HomeController(ICategoryRepository categoryRepository)
        {
            this.categoryRepository = categoryRepository;
        }
        
        [Route("index")]
        [Route("")]
        public IActionResult Index()
        {
            ViewBag.categories = this.categoryRepository.GetLimitedCategories();
            
            return View();
        }
    }
}