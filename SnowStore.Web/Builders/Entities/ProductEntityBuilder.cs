using System;
using SnowStore.Core.Entities;
using SnowStore.Core.Repositories;
using SnowStore.Core.ValueObject;
using SnowStore.Web.ViewModels.ResourcesManage;

namespace SnowStore.Web.Builders.Entities
{
    public class ProductEntityBuilder
    {
        private ICategoryRepository categoryRepository;
        
        public ProductEntityBuilder(ICategoryRepository categoryRepository)
        {
            this.categoryRepository = categoryRepository;
        }
        
        public Product Build(ProductViewModel productViewModel)
        {
            return new Product
            {
                Name = productViewModel.Name,
                Description = productViewModel.Description,
                Image = productViewModel.Image,
                Price = GetPrice(productViewModel.Price),
                Category = GetCategory(productViewModel.CategoryId),
                CreatedAt = DateTime.Now
            };
        }

        private Price GetPrice(decimal price)
        {
            return new Price
            {
                Value = price,
                Currency = Price.DefaultCurrency
            };
        }

        private Category GetCategory(int categoryId)
        {
            return categoryRepository.FindOneBy(categoryId);
        }
    }
}