
namespace SnowStore.Web.ViewModels.ResourcesManage
{
    public class ProductViewModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        // можно ли использовать трансформатор на вью модель, чтобы тут было поле с объектом
        // public Category Category { get; set; }?
        public int CategoryId { get; set; }
        public string Image { get; set; }
        public decimal Price { get; set; }
    }
}