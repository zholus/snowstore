using System;
using System.ComponentModel.DataAnnotations;
using SnowStore.Core.ValueObject;

namespace SnowStore.Core.Entities
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Category Category { get; set; }
        public string Image { get; set; }
        public Price Price { get; set; }
        [DataType(DataType.Date)]
        public DateTime CreatedAt { get; set; }
    }
}