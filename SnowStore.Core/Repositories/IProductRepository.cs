using System.Collections.Generic;
using SnowStore.Core.Entities;

namespace SnowStore.Core.Repositories
{
    public interface IProductRepository
    {
        bool Add(Product product);
        List<Product> FindAllByCategory(Category category);
        Product FindOneBy(int productId);
    }
}