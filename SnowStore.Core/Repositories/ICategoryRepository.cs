using System.Collections.Generic;
using SnowStore.Core.Entities;

namespace SnowStore.Core.Repositories
{
    public interface ICategoryRepository
    {
        List<Category> GetLimitedCategories(int limit = 5);
        List<Category> GetAllCategories();
        bool Add(Category category);
        Category FindOneBy(int categoryId);
    }
}