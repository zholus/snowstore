namespace SnowStore.Core.ValueObject
{
    public class Price
    {
        public const string DefaultCurrency = "UAH";
        
        public decimal Value { get; set; }
        public string Currency { get; set; }
    }
}